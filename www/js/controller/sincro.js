/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getSincroFromService() {

    var sqlInserts = [];

    $.ajax({
        url: "http://192.168.1.31:8085/SICRESAPWS/rest/registro/reparto/sincronizacion/",
        data: null,
        type: "POST",
        dataType: 'json',
        // código a ejecutar si la petición es satisfactoria;
        // la respuesta es pasada como argumento a la función
        success: function (json) {

            //creacion y retorno de un array con las sentencias creadas a partir del json obtenido 
            sqlInserts = createInserts(json);
            return sqlInserts;
        },
        // código a ejecutar si la petición falla;
        // son pasados como argumentos a la función
        // el objeto de la petición en crudo y código de estatus de la petición
        error: function (xhr, status) {
            console.log("Ocurrió un error en la obtención de data");
            return sqlInserts;
        },
        // código a ejecutar sin importar si la petición falló o no
        complete: function (xhr, status) {
            console.log("Peticion realizada");
        }
    });
}

function createInserts(json) {
    var sql = [];
    var jsonResult = {
        "SICRESAPT_MOTIVO": [
            {
                "MOTIN_CODIGO": "1010",
                "MOTIV_DESCRIPCION": "Motivo Nro 1"
            },
            {
                "MOTIN_CODIGO": "1020",
                "MOTIV_DESCRIPCION": "Motivo Nro 2"
            },
            {
                "MOTIN_CODIGO": "11122",
                "MOTIV_DESCRIPCION": "Motivo Nro 3"
            }
        ],
        "SICRESAPT_ENTREGA": [
            {
                "ENTRV_DT": "211417",
                "ENTRV_POS_DOC_TRANSP": "1",
                "ENTRV_COD_ENTREGA": "11122",
                "ENTRV_COD_PEDIDO": "5111536327",
                "ENTRV_NRO_DOC": "11241",
                "ENTRV_COD_DEST_MERC": "1900027167",
                "ENTRV_DIRECCION": "Av. La Molina 142",
                "ENTRV_COD_CLIENTE": "3333333",
                "ENTRV_NOMBRE_CLIENTE": "Daniel Barquero",
                "ENTRD_FECHA_SALIDA": "03/08/2015",
                "ENTRD_FECHA_LLEGADA": "04/08/2015",
                "ENTRN_PESO": "4.231",
                "ENTRV_PLACA": "B83-992",
                "ENTRV_COD_EMPR_TRANSP": "123124",
            },
            {
                "ENTRV_DT": "235235",
                "ENTRV_POS_DOC_TRANSP": "2",
                "ENTRV_COD_ENTREGA": "3353",
                "ENTRV_COD_PEDIDO": "53512",
                "ENTRV_NRO_DOC": "213123",
                "ENTRV_COD_DEST_MERC": "44422|",
                "ENTRV_DIRECCION": "Av. Separadora Industrial 444",
                "ENTRV_COD_CLIENTE": "4444444",
                "ENTRV_NOMBRE_CLIENTE": "Manuel Torres",
                "ENTRD_FECHA_SALIDA": "03/08/2015",
                "ENTRD_FECHA_LLEGADA": "04/08/2015",
                "ENTRN_PESO": "4.231",
                "ENTRV_PLACA": "B83-192",
                "ENTRV_COD_EMPR_TRANSP": "333222",
            }
        ],
        "SICRESAPT_MATERIAL": [
            {
                "MATEV_DT": "801313",
                "MATEV_COD_ENTREGA": "23523",
                "MATEV_POSICION": "1",
                "MATEV_SKU": "CODIGO1",
                "MATEV_DESC_PRODUCTO": "Producto1",
                "MATEV_UNIDADES": "VARILLAS",
                "MATEN_CANTIDAD": "20",
                "MATEN_PESO_UNIDAD": "0.4",
                "MATEV_UNIDAD_PESO": "",
            },
            {
                "MATEV_DT": "232121",
                "MATEV_COD_ENTREGA": "35533",
                "MATEV_POSICION": "2",
                "MATEV_SKU": "CODIGO2",
                "MATEV_DESC_PRODUCTO": "Producto2",
                "MATEV_UNIDADES": "VARILLAS",
                "MATEN_CANTIDAD": "3",
                "MATEN_PESO_UNIDAD": "0.44",
                "MATEV_UNIDAD_PESO": "",
            }
        ],
    };

    $.each(jsonResult.SICRESAPT_MOTIVO, function (key, data) {
        sql.push("INSERT INTO SICRESAPT_MOTIVO (MOTIN_CODIGO, MOTIV_DESCRIPCION)" + " VALUES('" +
                data.MOTIN_CODIGO + "','" +
                data.MOTIV_DESCRIPCION + "')");
    });

    $.each(jsonResult.SICRESAPT_MATERIAL, function (key, data) {
        sql.push("INSERT INTO SICRESAPT_MATERIAL (MATEV_DT, MATEV_COD_ENTREGA," +
                "MATEV_POSICION, MATEV_SKU, MATEV_DESC_PRODUCTO, MATEV_UNIDADES," +
                "MATEN_CANTIDAD,MATEN_PESO_UNIDAD, MATEV_UNIDAD_PESO) VALUES('" +
                data.MATEV_DT + "','" +
                data.MATEV_COD_ENTREGA + "','" +
                data.MATEV_POSICION + "','" +
                data.MATEV_SKU + "','" +
                data.MATEV_DESC_PRODUCTO + "','" +
                data.MATEV_UNIDADES + "','" +
                data.MATEN_CANTIDAD + "','" +
                data.MATEN_PESO_UNIDAD + "','" +
                data.MATEV_UNIDAD_PESO + "')");
    });

    $.each(jsonResult.SICRESAPT_ENTREGA, function (key, data) {
        sql.push("INSERT INTO SICRESAPT_ENTREGA (RREPN_ID_REG_REPARTO, RORDN_ID_REG_ORDEN, " +
                "ENTRV_DT, ENTRV_POS_DOC_TRANSP, ENTRV_COD_ENTREGA, ENTRV_COD_PEDIDO, " +
                "ENTRV_NRO_DOC, ENTRV_COD_DEST_MERC, ENTRV_DIRECCION, ENTRV_COD_CLIENTE, " +
                "ENTRV_NOMBRE_CLIENTE, ENTRD_FECHA_SALIDA, ENTRD_FECHA_LLEGADA, ENTRN_PESO, " +
                "ENTRV_PLACA, ENTRV_COD_EMPR_TRANSP)" +
                " VALUES('" +
                data.RREPN_ID_REG_REPARTO + "','" +
                data.RORDN_ID_REG_ORDEN + "','" +
                data.ENTRV_DT + "','" +
                data.ENTRV_POS_DOC_TRANSP + "','" +
                data.ENTRV_COD_ENTREGA + "','" +
                data.ENTRV_COD_PEDIDO + "','" +
                data.ENTRV_NRO_DOC + "','" +
                data.ENTRV_COD_DEST_MERC + "','" +
                data.ENTRV_DIRECCION + "','" +
                data.ENTRV_COD_CLIENTE + "','" +
                data.ENTRV_NOMBRE_CLIENTE + "','" +
                data.ENTRD_FECHA_SALIDA + "','" +
                data.ENTRD_FECHA_LLEGADA + "','" +
                data.ENTRN_PESO + "','" +
                data.ENTRV_PLACA + "','" +
                data.ENTRV_COD_EMPR_TRANSP + "')");
    });

    return sql;
}

